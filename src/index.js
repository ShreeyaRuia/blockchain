import express from 'express';
import logger from './utils/logger';
import { toHandle404Error, toHandle500Error } from './middlewares/error';

const app = express();

const port = 3000;

app.use(express.json()); // for parsing application/json
app.use(express.urlencoded({ extended: true }));

app.use(toHandle500Error);
app.use(toHandle404Error);

app.listen(port, () => {
  logger.info(`Example app listening at http://localhost:${port}`);
});

export default app;
