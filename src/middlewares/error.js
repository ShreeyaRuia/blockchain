/* eslint-disable no-unused-vars */
import logger from '../utils/logger';

/**
 * @param  {Request} req
 * @param  {Response} res
 * @param  {Function} next
 */
function toHandle404Error(req, res, next) {
  res.status(404).send('Page not found');
  logger.info('ERROR Response sent succesfully');
}

/**
 * @param  {Error} error
 * @param  {Request} req
 * @param  {Response} res
 * @param  {Function} next
 */
function toHandle500Error(error, req, res, next) {
  res.status(500).send('Internal server Error');
  logger.info(error);
  logger.info('ERROR Response sent succesfully');
}

export { toHandle404Error, toHandle500Error };
